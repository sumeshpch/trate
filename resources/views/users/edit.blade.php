@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif

                    <form method="post" action="{{ url('updateprofile') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('patch') }}

                        <div class="form-group">
                            <label for="imageInput">Name</label>
                            <input type="text" name="name"  value="{{ $user->name }}" />
                        </div>
                        <div class="form-group">
                            <label for="imageInput">Email</label>
                            <input type="text" name="name"  value="{{ $user->email }}" disabled="disabled" readonly="readonly" />
                        </div>
                        <div class="form-group">
                            <label for="imageInput">Role</label>
                            <input type="text" name="name"  value="{{ $user->role }}" disabled="disabled" readonly="readonly" />
                        </div>
                        <div class="form-group">
                            <img src="{{ url('/') }}/userimages/{{ $user->userimage }}" width="200" />
                            <input data-preview="#preview" name="input_image" type="file" id="imageInput">
                            <img class="col-sm-6" id="preview"  src="">
                            <p class="help-block">Please insert image in jpeg,png,jpg,gif with maximum 2MB size</p>
                        </div>
                        <div class="form-group">
                            <input type="submit" />
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection