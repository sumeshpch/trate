<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     * php artisan db:seed --class=UsersTableSeeder
     * @return void
     */
    public function run() {
        DB::table('users')->insert([
                [
                'name' => 'Administrator',
                'email' => 'admin@trate.com',
                'password' => bcrypt('password'),
                'role' => 'admin',
                'userimage' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
                [
                'name' => 'Sumesh',
                'email' => 'sumeshpch@gmail.com',
                'password' => bcrypt('password'),
                'role' => 'dealer',
                'userimage' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        ]);
    }

}
