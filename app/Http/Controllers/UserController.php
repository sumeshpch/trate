<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function edit(User $user) {
        $user = Auth::user();
        return view('users.edit', compact('user'));
    }

    public function update(Request $request) {
        $this->validate(request(), [
            'name' => 'required',
            'input_image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        $user = Auth::user();
        $name = "";

        if ($request->hasFile('input_image')) {
            $image = $request->file('input_image');
            $name = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/userimages');
            $image->move($destinationPath, $name);
        }
        $user->userimage = $name;
        $user->name = request('name');

        $user->save();

        return back();
    }

}
